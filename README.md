# Anonimo

Server-side software which the Anonimo platform runs on

## Getting Started

These instructions will get you a copy of the server up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Install the following dependencies in the listed order before continuing with your installation.


2. [golang](https://golang.org/)
3. [golang/dep](https://github.com/golang/dep)


### Installing

Clone the server repository onto your local machine.

```
git clone https://gitlab.com/Anonimo/server $GOPATH/src/anonimo/server && cd $GOPATH/src/anonimo/server
```

Install the server dependencies.

```
cd server && dep ensure && cd ..
```

## Running the project

To run the server execute the following command.

```
cd server && ./run.sh
```

## Contributing

Make sure to write code according to the following guidelines.

1. [GoLang](https://github.com/golang/go/wiki/CodeReviewComments)
